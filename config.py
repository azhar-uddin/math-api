import os
from os.path import join, dirname
from dotenv import load_dotenv

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

# For optional config variables use os.getenv('key') or os.environ.get('key')
# For mandatory config variables use os.eniron['key']

# Application
APP_NAME = os.environ.get('APP_NAME', 'APP_NAME')
