""" init file for app """
from flask import Flask
from app.api import BLUEPRINTS

def create_app(test_config=None):
    """ init app """
    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        app.config.from_object('config')
    else:
        # load the test config if passed
        app.config.from_mapping(test_config)

    app.config['BUNDLE_ERRORS'] = True

    # register blueprints
    for blueprint, kwargs in BLUEPRINTS:
        app.register_blueprint(blueprint, **kwargs)

    return app
