from flask import current_app as app

from app.api.endpoints import endpoints_bp

BLUEPRINTS = [
    (endpoints_bp, {"url_prefix": "/api"})
]