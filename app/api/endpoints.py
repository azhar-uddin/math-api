from http import HTTPStatus
from flask import Blueprint
from flask import current_app as app
from flask.json import jsonify
import math
from app.lib.constants import DEFAULT_ERROR_MSG

endpoints_bp = Blueprint('endpoints', __name__)


def fib_recursive(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib_recursive(n-1) + fib_recursive(n-2)

@endpoints_bp.route('/fibonacci/<input>')
def fibonacci(input):
    try:
        return jsonify([fib_recursive(i) for i in range(int(input))])
    except Exception as ex:
        app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': DEFAULT_ERROR_MSG}), HTTPStatus.INTERNAL_SERVER_ERROR


@endpoints_bp.route('/factorial/<input>')
def factorial(input):
    try:
        return jsonify(math.factorial(int(input)))
    except Exception as ex:
        app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': DEFAULT_ERROR_MSG}), HTTPStatus.INTERNAL_SERVER_ERROR