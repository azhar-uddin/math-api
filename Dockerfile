FROM python:3.7

RUN mkdir -p math

RUN chmod 755 math

WORKDIR /math

RUN pip3 install pipenv

COPY . .

RUN pipenv install --system --deploy --ignore-pipfile

EXPOSE 5000

CMD flask run --host=0.0.0.0
