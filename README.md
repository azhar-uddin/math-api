# MathAPI

#### Environment
```commandline
python 3.7
``` 

#### Start Application in Development

Build the Docker Image and Run
```commandline
./run.sh
```

Call the Endpoints from Postman to get the response. Example

```commandline
http://localhost:5000/api/fibonacci/6
http://localhost:5000/api/factorial/6

```
