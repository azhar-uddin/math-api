#!/bin/bash
# utility script to build and publish a docker image to a registry

# configure registry and image path
REGISTRY=registry.gitlab.com
NAMESPACE=azhar-uddin
IMAGE_NAME=math-api
IMAGE_TAG=latest

# login to the registry
docker login $REGISTRY

# build the docker image
docker build -t $REGISTRY/$NAMESPACE/$IMAGE_NAME:$IMAGE_TAG .

# publish/upload the image
docker push $REGISTRY/$NAMESPACE/$IMAGE_NAME:$IMAGE_TAG
